# DebConf18 Ledger

*   The format of ledger is defined in http://ledger-cli.org/.
*   `OCF.tw` is ledger for OCF.tw.

accounts.inc
 - List of accounts
beerbar.ledger
 - Beer accounting
budget.ledger
 - Current budget DPL approved
docs
 - Documents from goverment funds
expenses.ledger
 - Expenses
incomes.ledger
 - Incomes
journal.ledger
 - expenses + incomes
README.md
receipts
 - pictures of receipts
 - keep paper receipts for reimbursement other than SPI
 - NCHC requires quotes for video equipment rental
