commodity USD
   note US dollars
   format USD 1,000.00
   alias $

commodity CAD
   note Canadian dollars
   format CAD 1,000.00

commodity EUR
   note Euros
   format EUR 1,000.00

commodity CHF
   note Swiss francs
   format CHF 1,000.00

commodity MXN
   note Mexican pesos
   format MXN 1,000.00

commodity TWD
   note Taiwan New Dollar
   format TWD 1,000
   default
